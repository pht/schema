import typing
import rdflib
import pathlib
from datetime import datetime

from mako.template import Template
from mako import exceptions
from specificationsynth.parser import DescriptionParser
from specificationsynth.descriptions import ConcreteClassDescription, ConcreteObjectPropertyDescription, Description
from specificationsynth.resolvement import resolve

ONTOLOGY_PATH = "1_0.owl"
CONSTRAINTS_PATH ="./shapes"
MAKO_TEMPLATE_PATH = "1_0.mako"

PHT = rdflib.Namespace("http://schema.padme-analytics.de#")

def load_onto():
    g = rdflib.Graph()
    with open(ONTOLOGY_PATH, "r") as f:
        g.parse(f, format="ttl")
    return g

def load_constraints():
    g = rdflib.Graph()
    p = pathlib.Path(CONSTRAINTS_PATH)
    for shape_p in p.iterdir():
        with shape_p.open("r") as f:
            g.parse(f)
    return g

def extract_object_properties(descriptions: typing.List[ConcreteClassDescription]):
    """
    Extract the object properties based on the assumption that if they have the same iri, that they are the same
    """
    object_properties = set()
    for cd in descriptions:
        object_properties.update(cd.object_properties)
    return list(object_properties)

def extract_data_properties(descriptions: typing.List[ConcreteClassDescription]):
    """
    Extract the data properties based on the assumption that if they have the same iri, that they are the same
    """
    data_properties = set()
    for cd in descriptions:
        data_properties.update(cd.data_properties)
    return list(data_properties)

def add_definitions(resources: typing.List[Description], definitions: rdflib.Graph):
    for resource in resources:
        resource.definitions = list(definitions.objects(rdflib.URIRef(resource.iri), rdflib.URIRef("http://www.w3.org/ns/prov#definition")))

def load_template():
    return Template(filename=MAKO_TEMPLATE_PATH)

def render_template(classes, object_properties, data_properties):
    template = load_template()
    dt = datetime.now()
    try:
        return template.render(classes=classes, data_properties=data_properties, object_properties=object_properties, dt=dt.isoformat())
    except:
        print("error while rendering:")
        print(exceptions.text_error_template().render())

definitions = load_onto()
constraints = load_constraints()

parser = DescriptionParser(definitions, constraints, True)

classes = parser.parse()

resolved_classes = list(map(lambda x: resolve(x), classes.values()))
add_definitions(resolved_classes, definitions)

# we alter the class here since we may include the specificationsyth package 
# as package dependency in the future and may no longer access to the code then

def get_CURIE_additional(self: ConcreteClassDescription):
        """Dirty getter for CURIE"""
        return "pht:" + self.get_defining_part()

def get_defining_part_additional(self: ConcreteClassDescription):
    return self.iri[len(str(PHT)):]

def get_label_additional(self: ConcreteClassDescription):
    return self.labels[0] if len(self.labels) > 0 else self.get_defining_part()

Description.get_CURIE = get_CURIE_additional
Description.get_defining_part = get_defining_part_additional
Description.get_label = get_label_additional


object_properties = extract_object_properties(resolved_classes)
data_properties = extract_data_properties(resolved_classes)
add_definitions(object_properties, definitions)
add_definitions(data_properties, definitions)

rendered_html = render_template(resolved_classes, object_properties, data_properties)
with open("rendered_html.html","w") as f:
    f.write(rendered_html)