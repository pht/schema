"""
Contains descriptions classes which contain all information extracted from a schema.
Descriptions are very non-constrained classes, where everything can be None.
They start completely uninitialised and are normally populate with information during parsing of the given informaiton graphs.
"""
import typing
from dataclasses import dataclass, field
from .statement import StatementCollection, HasSuperClassStatement, CardinalityRestrictionStatement, PropertyHasClassRangeStatement, PropertyHasDatatypeRangeStatement
@dataclass
class Description:
    iri: str
    # labels can be a simple list
    labels: typing.List[str] = field(default_factory=list)
    comments: typing.List[str] = field(default_factory=list)

    def __hash__(self):
        return hash(self.iri)

@dataclass
class PropertyDescription(Description):
    """
    Describes a property in the context of a class
    """
    cardinality: StatementCollection[CardinalityRestrictionStatement] = field(default_factory=StatementCollection)

@dataclass
class ObjectPropertyDescription(PropertyDescription):
    range: StatementCollection[PropertyHasClassRangeStatement] = field(default_factory=StatementCollection)
@dataclass
class DataPropertyDescription(PropertyDescription):
    range: StatementCollection[PropertyHasClassRangeStatement] = field(default_factory=StatementCollection)
@dataclass
class ClassDescription(Description):
    object_properties: typing.List[ObjectPropertyDescription] = field(default_factory=list)
    data_properties: typing.List[DataPropertyDescription] = field(default_factory=list)

    direct_superclasses: StatementCollection[HasSuperClassStatement] = field(default_factory=StatementCollection)


@dataclass
class ConcretePropertyDescription(Description):
    min_cardinality: typing.Optional[int] = None
    max_cardinality: typing.Optional[int] = None

@dataclass
class ConcreteObjectPropertyDescription(ConcretePropertyDescription):
    """
    Concrete Object Property Description
    range can be Class Description (internal) or str (external)
    multiple values are seen as Union since is the most used case

    """
    range: typing.List[typing.Union[ClassDescription, str]] = field(default_factory=list)

    def __hash__(self):
        return hash(self.iri)

@dataclass
class ConcreteDataPropertyDescription(ConcretePropertyDescription):
    """
    Concrete Data Property Description
    range is a IRI of a datatype
    multiple values are seen as Union
    """
    range: typing.List[str] = field(default_factory=list)

    def __hash__(self):
        return hash(self.iri)


@dataclass
class ConcreteClassDescription(Description):
    object_properties: typing.List[ConcreteObjectPropertyDescription] = field(default_factory=list)
    data_properties: typing.List[ConcreteDataPropertyDescription] = field(default_factory=list)

    # use description as type, since ConcreteClass does not work here
    direct_superclasses: typing.List[Description] = field(default_factory=list)





