import typing

import rdflib

from rdflib.namespace import SH, RDF

from .statement import MaxCardinalityRestrictionStatement, MinCardinalityRestrictionStatement, PropertyHasInternalRangeStatement, PropertyHasDatatypeRangeStatement, PropertyHasExternalRangeStatement
from .descriptions import ClassDescription, PropertyDescription, DataPropertyDescription, ObjectPropertyDescription
from .util import filter_int_literals

class ClassConstraintParser:
    """
    Parser which extracts information for one particular class
    """

    def __init__(self, graph: rdflib.Graph, class_: ClassDescription, context_dict: typing.Dict[str, ClassDescription]):
        self._graph = graph
        self._class = class_
        self._context_dict = context_dict

    def get_shapes_for_class(self):
        return self._graph.subjects(SH.targetClass, rdflib.URIRef(self._class.iri))

    def check_property_affection(self, property: PropertyDescription, property_shape: rdflib.term.Node):
        """
        Checks whether the given property is affected by the given shape
        """
        return rdflib.URIRef(property.iri) in set(self._graph.objects(property_shape, SH.path))

    def get_shapes_for_property(self, property: PropertyDescription, in_shape: rdflib.term.Node):
        """
        Returns list of affecting for this property
        """
        return filter(lambda x: self.check_property_affection(property, x), self._graph.objects(in_shape, SH.property))

    def get_min_constraints(self, property_shape: rdflib.term.Node):
        """
        Returns the min constraint values defined in the property shape
        """
        return filter_int_literals(self._graph.objects(property_shape, SH.minCount))

    def get_max_constraints(self, property_shape: rdflib.term.Node):
        """
        Returns the min constraint values defined in the property shape
        """
        return filter_int_literals(self._graph.objects(property_shape, SH.maxCount))

    def get_cardinality_constraints_statements(self, property_shape: rdflib.term.Node):
        mins = [MinCardinalityRestrictionStatement(int(cardinality), self._graph) for cardinality in self.get_min_constraints(property_shape)]
        maxs = [MaxCardinalityRestrictionStatement(int(cardinality), self._graph) for cardinality in self.get_max_constraints(property_shape)]
        return mins + maxs

    def get_range_constraints_statements(self, property_shape: rdflib.term.Node):
        """
        Returns list of statements retrieved from the property shape
        """
        range_constraints = []
        for class_constraint in self._graph.objects(property_shape, SH["class"]):
            if isinstance(class_constraint, rdflib.URIRef):
                if str(class_constraint) in self._context_dict:
                    range_constraints.append(PropertyHasInternalRangeStatement(str(class_constraint), self._graph))
                else:
                    range_constraints.append(PropertyHasExternalRangeStatement(str(class_constraint), self._graph))

        for datatype_constraint in self._graph.objects(property_shape, SH.datatype):
            range_constraints.append(PropertyHasDatatypeRangeStatement(str(datatype_constraint), self._graph))

        return range_constraints
    def parse_property_shape_for_property(self, property: typing.Union[DataPropertyDescription, ObjectPropertyDescription], property_shape: rdflib.term.Node):
        """
        Parse a property shape for a specific property
        """
        for s in self.get_cardinality_constraints_statements(property_shape):
            property.cardinality.append(s)
        for s in self.get_range_constraints_statements(property_shape):
            property.range.append(s)

    def parse_shape(self, shape: rdflib.term.Node):
        for obj_prop in self._class.object_properties:
            for prop_shape in self.get_shapes_for_property(obj_prop, shape):
                self.parse_property_shape_for_property(obj_prop, prop_shape)

        for data_prop in self._class.data_properties:
            for prop_shape in self.get_shapes_for_property(data_prop, shape):
                self.parse_property_shape_for_property(data_prop, prop_shape)

    def populate_class_in_context(self):
        """
        Parse the constraints defined in the given graph
        """
        for shape in self.get_shapes_for_class():
            self.parse_shape(shape)