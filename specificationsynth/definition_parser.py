"""
Contains methods for extracing information from definitions
"""
import typing

import rdflib

from .descriptions import ClassDescription, ObjectPropertyDescription, DataPropertyDescription, PropertyDescription
from .statement import PropertyHasInternalRangeStatement, PropertyHasExternalRangeStatement, \
    PropertyHasDatatypeRangeStatement, HasSuperClassStatement
from .util import subject, filter_string_literals

INSTANTIATION_PREDICATE = rdflib.namespace.RDF.type
DOMAIN_PREDICATE = rdflib.namespace.RDFS.domain
RANGE_PREDICATE = rdflib.namespace.RDFS.range
LABEL_PREDICATE = rdflib.namespace.RDFS.label
COMMENT_PREDICATE = rdflib.namespace.RDFS.comment
SUBCLASS_STATEMENT = rdflib.namespace.RDFS.subClassOf

def get_class_instantiation_object(OWL_definitions):
    return rdflib.OWL.Class if OWL_definitions else rdflib.RDFS.Class


def get_object_property_object(OWL_definitions):
    return rdflib.OWL.ObjectProperty if OWL_definitions else rdflib.RDF.Property


def get_data_property_object(OWL_definitions):
    return rdflib.OWL.DatatypeProperty if OWL_definitions else rdflib.RDF.Property


def get_classes(graph: rdflib.Graph, OWL_definitions=False, include_BNodes=False):
    """
    Generate a list of iris that are defined as classes.
    Normally initial step in every parsing
    """
    return filter(lambda x: include_BNodes or not isinstance(x, rdflib.BNode), map(subject, graph.triples(
        (None, INSTANTIATION_PREDICATE, get_class_instantiation_object(OWL_definitions)))))


def get_object_properties(graph: rdflib.Graph, OWL_definitions=False, include_BNodes=False):
    """
    Generate a list of iris that are defined as object properties
    """
    return filter(lambda x: include_BNodes or not isinstance(x, rdflib.BNode), map(subject, graph.triples(
        (None, INSTANTIATION_PREDICATE, get_object_property_object(OWL_definitions)))))


def get_data_properties(graph: rdflib.Graph, OWL_definitions=False, include_BNodes=False):
    """
    Generate a list of iris that are defined as data properties
    """
    return filter(lambda x: include_BNodes or not isinstance(x, rdflib.BNode), map(subject, graph.triples(
        (None, INSTANTIATION_PREDICATE, get_data_property_object(OWL_definitions)))))


def get_properties_for_class_as_domain_triples(graph: rdflib.Graph, cls):
    """
    Generates a list of triples that describe domain relationships
    """
    return graph.triples((None, DOMAIN_PREDICATE, rdflib.URIRef(cls)))


def get_properties_for_class_as_range_triples(graph: rdflib.Graph, cls):
    """
    Generates a list of triples that describe range relationships
    """
    return graph.triples((None, RANGE_PREDICATE, rdflib.URIRef(cls)))


def get_range_for_property_as_triples(graph: rdflib.Graph, prop):
    """
    Generates a list of triples that describe range of a given property iri
    """
    return graph.triples((rdflib.URIRef(prop), RANGE_PREDICATE, None))



def get_labels(graph: rdflib.Graph, resource: rdflib.URIRef):
    """
    Returns an iteration of strings of valid labels for the given resource
    """
    return filter_string_literals(graph.objects(resource, LABEL_PREDICATE))


def get_comments(graph: rdflib.Graph, resource: rdflib.URIRef):
    """
    Returns an iteration of comments for the given resource
    """
    return filter_string_literals(graph.objects(resource, COMMENT_PREDICATE))

def get_direct_superclasses(graph: rdflib.Graph, class_: rdflib.URIRef):
    """
    Returns a list of classes which are defined as direct superclasses of the given class
    """
    return graph.objects(class_, SUBCLASS_STATEMENT)

def get_class_dict(graph: rdflib.Graph, OWL_definitions=False):
    """
    Generate a dictionary of (empty) class descriptions
    """
    m = {}
    [m.update({str(iri): ClassDescription(str(iri))}) for iri in
     get_classes(graph, OWL_definitions, include_BNodes=False)]
    return m


class ClassParser:
    """
    Parser for a single class in the context of a class dictionary
    """

    def __init__(self, graph: rdflib.Graph, class_: ClassDescription, context_dict: typing.Dict[str, ClassDescription],
                 OWL_definitions=False):
        self._graph = graph
        self._class = class_
        self._context_dict = context_dict
        self._OWL_definitions = OWL_definitions

    def get_range_for_property(self, property: rdflib.URIRef):
        """
        Generates a list of nodes that describe range of a given property iri
        """
        return self._graph.objects(rdflib.URIRef(property), RANGE_PREDICATE)

    def populate_property_with_supporting_information(self, property: PropertyDescription):
        """
        Populate a given property description with additional information such as labels or comments
        """
        property.labels.extend(get_labels(self._graph, rdflib.URIRef(property.iri)))
        property.comments.extend(get_comments(self._graph, rdflib.URIRef(property.iri)))

    def get_object_property_in_class_context(self, iri: rdflib.URIRef):
        """
        Returns a Object property description in context of a class based on the given iri of the object property
        """
        o = ObjectPropertyDescription(str(iri))
        self.populate_property_with_supporting_information(o)
        for range_iri in self.get_range_for_property(iri):
            if str(range_iri) in self._context_dict:
                o.range.append(PropertyHasInternalRangeStatement(self._context_dict[str(range_iri)], self._graph))
            else:
                o.range.append(PropertyHasExternalRangeStatement(str(iri), self._graph))
        return o

    def get_data_property_in_class_context(self, iri: rdflib.URIRef):
        """
        Returns a Data property description object in the context of a class based on the given iri of the data property
        """
        p = DataPropertyDescription(str(iri))
        self.populate_property_with_supporting_information(p)
        for range_iri in self.get_range_for_property(iri):
            p.range.append(PropertyHasDatatypeRangeStatement(str(range_iri), self._graph))
        return p

    def get_data_property_iris_for_class(self):
        return filter(lambda x: (x, DOMAIN_PREDICATE, rdflib.URIRef(self._class.iri)) in self._graph,
                      self._graph.subjects(INSTANTIATION_PREDICATE, get_data_property_object(self._OWL_definitions)))

    def get_object_property_iris_for_class(self):
        """
        Returns all object properties where the given class is the domain
        """
        return filter(lambda x: (x, DOMAIN_PREDICATE, rdflib.URIRef(self._class.iri)) in self._graph,
                      self._graph.subjects(INSTANTIATION_PREDICATE, get_object_property_object(self._OWL_definitions)))

    def populate_class_with_object_properties(self):
        for object_property_iri in filter(lambda x: isinstance(x, rdflib.URIRef),
                                          self.get_object_property_iris_for_class()):
            assert (isinstance(object_property_iri, rdflib.URIRef))
            self._class.object_properties.append(self.get_object_property_in_class_context(object_property_iri))

    def populate_class_with_data_properties(self):
        for data_property_iri in filter(lambda x: isinstance(x, rdflib.URIRef),
                                        self.get_data_property_iris_for_class()):
            assert (isinstance(data_property_iri, rdflib.URIRef))
            self._class.data_properties.append(self.get_data_property_in_class_context(data_property_iri))

    def populate_class_with_properties(self):
        self.populate_class_with_object_properties()
        self.populate_class_with_data_properties()

    def get_superclass_statements(self):
        """
        Get statements describing the direct superclass
        """
        statements = []
        for s_iri in get_direct_superclasses(self._graph, rdflib.URIRef(self._class.iri)):
            if str(s_iri) in self._context_dict:
                statements.append(HasSuperClassStatement(self._context_dict[str(s_iri)], self._graph))

        return statements

    def populate_class_in_context(self):
        self.populate_class_with_properties()

        [self._class.direct_superclasses.append(s) for s in self.get_superclass_statements()]

        self._class.labels.extend(get_labels(self._graph, rdflib.URIRef(self._class.iri)))
        self._class.comments.extend(get_comments(self._graph, rdflib.URIRef(self._class.iri)))
