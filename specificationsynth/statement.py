"""
Contains the statement class for describing statements retrieved from the data graphs.
A statement class describes a statement about a value of a class or a property.
Subclasses of this class refines the statement with more semantic meaningful methods.
"""
import typing
import rdflib
import collections.abc





class Statement:
    """
    Statement class for expressing statements about characteristics of classes and properties
    """
    def __init__(self, value: typing.Any, *origin: typing.Union[typing.Tuple[rdflib.Graph], rdflib.Graph]):
        self._origin = origin
        self._value = value

    def get(self):
        """
        Getter with value conversion
        """
        return self._value

    def set(self, value):
        """
        Setter with value conversion for overwritting by subclasses
        """
        self._value = value

    def get_original(self):
        return self._value

    def resolve(self, other):
        return self

    def __add__(self, other):
        return self.resolve(other)

    def __eq__(self, other):
        if isinstance(other, Statement):
            return self._value == other._value

    def __str__(self):
        return str(self._value)

StatementType = typing.TypeVar("StatementType", bound=Statement)

class StatementCollection(typing.Generic[StatementType]):
    """
    A queryable Collection of statements
    """
    def __init__(self, statements=None):
        self._statements = list(statements) if statements is not None else []

    def get_statements(self, super_class=None) -> typing.List[StatementType]:
        if super_class is None:
            return self._statements
        else:
            return list(filter(lambda x: isinstance(x, super_class), self._statements))

    def append(self, s):
        self._statements.append(s)

    def empty(self):
        return len(self._statements) == 0

    def __eq__(self, other):
        if isinstance(other, StatementCollection):
            return self._statements == other._statements

        if isinstance(other, collections.abc.Iterable):
            return self._statements == list(other)
        raise NotImplemented()

    def __str__(self):
        return str(self._statements)

    def __iter__(self):
        return self._statements.__iter__()
class UnresolvableStatements(Exception):
    def __init__(self, s1, s2):
        self.s1 = s1
        self.s2 = s2



class HasSuperClassStatement(Statement):
    pass

class IsInPropertyDomainStatement(Statement):
    """
    Expresses that class is in the Domain of a property
    """
    def resolve(self, other):
        return self

class PropertyHasRangeStatement(Statement):
    """
    Expresses that a property has a specific range of another class.
    Multiple Range Statements express an Union of those
    """
class PropertyHasClassRangeStatement(PropertyHasRangeStatement):
    """
    Superclass statement for properties which have another class as range
    """

class PropertyHasExternalRangeStatement(PropertyHasClassRangeStatement):
    """
    Expresses that the property has a specific range of some class defined outside the current context
    """

class PropertyHasInternalRangeStatement(PropertyHasClassRangeStatement):
    """
    Expresses that the property has a specific range of some class which also exists in its schema.
    """

class PropertyHasDatatypeRangeStatement(PropertyHasRangeStatement):
    """
    Expresses that a property has a specific range of a datatype.
    Multiple Range statements for one property are currently not allowed
    """

    def resolve(self, other):
        if isinstance(other, PropertyHasDatatypeRangeStatement) or isinstance(other, PropertyHasRangeStatement):
            raise UnresolvableStatements(self, other)
        return self

class IsInPropertyRangeStatement(Statement):
    """
    Expresses that the class is in the Range of a Property
    """
    def resolve(self, other):
        return self

class CardinalityRestrictionStatement(Statement):
    """
    Expresses a general Cardinality Restriction for a property
    """

class MaxCardinalityRestrictionStatement(CardinalityRestrictionStatement):
    """
    Expresses a max cardinality restriction for a property
    """
    def resolve(self, other):
        if isinstance(other, MaxCardinalityRestrictionStatement):
            return MaxCardinalityRestrictionStatement(self._origin, min(self._value, other._value))
        elif isinstance(other, MinCardinalityRestrictionStatement):
            # commutative reduction
            return other.resolve(self)
        elif isinstance(other, MinMaxCardinalityRestrictionStatement):
            if self.get() < other.min():
                raise UnresolvableStatements(self, other)
            elif self.get() <= other.max():
                return MinMaxCardinalityRestrictionStatement((other.min(), self.get()), *(self._origin + other._origin))
            else:
                return other

        return self


class MinCardinalityRestrictionStatement(CardinalityRestrictionStatement):
    """
    Expresses a min cardinality restriction for a property
    """
    def resolve(self, other):
        if isinstance(other, MinCardinalityRestrictionStatement):
            return MinCardinalityRestrictionStatement(max(self._value, other._value), *(self._origin + other._origin))
        elif isinstance(other, MaxCardinalityRestrictionStatement):
            if other._value < self._value:
                raise UnresolvableStatements(self, other)
            else:
                return MinMaxCardinalityRestrictionStatement((self.get(), other.get()), *(self._origin + other._origin))
        elif isinstance(other, MinMaxCardinalityRestrictionStatement):
            if other.max() < self._value:
                raise UnresolvableStatements(self, other)
            elif other.min() < self._value:
                return MinCardinalityRestrictionStatement((self._value, other.max()), *(self._origin + other._origin))
            else:
                return other
        return self

class MinMaxCardinalityRestrictionStatement(CardinalityRestrictionStatement):
    """
    Expresse a cardinality restriction with both min and max values.
    The value is a tuple
    """

    def get(self) -> typing.Tuple[int, int]:
        return super().get()

    def set(self, value):
        if not isinstance(value, tuple) or len(value)!=2:
            raise ValueError("MinMaxCardinalityRestriction needs length 2 tuple.")

    def min(self) -> int:
        return self.get()[0]

    def max(self) -> int:
        return self.get()[1]
    def resolve(self, other):
        if isinstance(other, CardinalityRestrictionStatement) and not isinstance(other, MinMaxCardinalityRestrictionStatement):
            # resolvent is commutative
            return other.resolve(self)
        elif isinstance(other, MinMaxCardinalityRestrictionStatement):
            if other.max() < self.min() or other.min() > self.max():
                raise UnresolvableStatements(self, other)
            else:
                return MinMaxCardinalityRestrictionStatement((min(self.min(), other.min()),max(self.max(), other.max())), *(self._origin + other._origin))