import typing

import rdflib

def subject(x):
    return x[0]

def predicate(x):
    return x[1]

def object_(x):
    return x[2]

def filter_string_literals(iterator) -> typing.Iterator[str]:
    """
    Helper function for filtering iterator of nodes into an iterator of valid strings
    """
    return map(lambda x: str(x), filter(lambda x: isinstance(x, rdflib.Literal), iterator))

def filter_int_literals(iterator) -> typing.Iterator[int]:
    """
    Helper function for filtering iterator of nodes into an iterator of valid ints
    """
    return map(lambda x: int(x), filter(lambda x: isinstance(x, rdflib.Literal), iterator))