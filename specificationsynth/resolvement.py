"""
Contains method for statement reducing
"""
import typing
from functools import reduce

from .descriptions import ClassDescription, ConcreteClassDescription, ConcreteDataPropertyDescription, ConcreteObjectPropertyDescription, PropertyDescription, DataPropertyDescription, ObjectPropertyDescription
from .statement import *

def resolve_cardinality(property: PropertyDescription) -> typing.Tuple[typing.Optional[int], typing.Optional[int]]:
    """
    Resolves the cardinality statements of a property
    """
    if property.cardinality.empty():
        return (None, None)
    cardinality_statement = reduce(lambda x, y: x + y, property.cardinality)
    if isinstance(cardinality_statement, MinCardinalityRestrictionStatement):
        return (cardinality_statement.get(), None)
    if isinstance(cardinality_statement, MaxCardinalityRestrictionStatement):
        return (None, cardinality_statement.get())
    if isinstance(cardinality_statement, MinMaxCardinalityRestrictionStatement):
        return cardinality_statement.get()

def resolve_superclasses(superclass_statements: StatementCollection[HasSuperClassStatement]) -> typing.List[ClassDescription]:
    """
    Resolve the superclass statements
    Transforms the lists of statements into a concrete list of superclasses
    """
    return list(map(lambda x: x.get(), superclass_statements))
def resolve_data_property(property: DataPropertyDescription):
    """
    Resolves the range information of a data property
    """
    cd = ConcreteDataPropertyDescription(property.iri, property.labels, property.comments)
    range = []
    for r in property.range:
        range.append(r.get())
    cd.range = range
    return cd


def resolve_object_property(property: ObjectPropertyDescription):
    """
    Resolves the range information of an object property
    """
    cd = ConcreteObjectPropertyDescription(property.iri, property.labels, property.comments)
    range = []
    for r in property.range:
        range.append(r.get())
    cd.range = range
    return cd

def resolve_specialised_property(property: PropertyDescription) -> typing.Union[ConcreteObjectPropertyDescription, ConcreteDataPropertyDescription]:
    if isinstance(property, DataPropertyDescription):
        return resolve_data_property(property)
    if isinstance(property, ObjectPropertyDescription):
        return resolve_object_property(property)
    raise ValueError("Unknown property type: " + str(type(property)))

def resolve_property(property: PropertyDescription):
    cd = resolve_specialised_property(property)
    cd.min_cardinality, cd.max_cardinality = resolve_cardinality(property)

    return cd

def resolve(description: ClassDescription):
    cd = ConcreteClassDescription(description.iri, description.labels, description.comments)
    cd.object_properties = list(map(lambda x: resolve_property(x), description.object_properties))
    cd.data_properties = list(map(lambda x: resolve_property(x), description.data_properties))

    cd.direct_superclasses = resolve_superclasses(description.direct_superclasses)

    return cd
