"""
Implement parser class
"""
import typing

import rdflib

from .definition_parser import ClassParser, get_class_dict
from .constraint_parser import ClassConstraintParser
from .descriptions import ClassDescription
class DescriptionParser:
    """
    Parser class for parsing schema files into descriptions
    """

    def __init__(self, definitions: rdflib.Graph, constraints: rdflib.Graph, OWL_definitions = False):
        self._definitions = definitions
        self._constraints = constraints
        self._OWL_definitions = OWL_definitions

    def parse(self) -> typing.Dict[str, ClassDescription]:
        """
        Parse the graphs and return a dictionary containing the class descriptions
        """
        _class_dict = self.get_class_descriptions()

        for _class in _class_dict.values():
            parser = ClassParser(self._definitions, _class, _class_dict, self._OWL_definitions)
            parser.populate_class_in_context()

        for _class in _class_dict.values():
            parser = ClassConstraintParser(self._constraints, _class, _class_dict)
            parser.populate_class_in_context()

        return _class_dict

    def get_class_descriptions(self):
        _class_dict = get_class_dict(self._definitions, self._OWL_definitions)


        return _class_dict

