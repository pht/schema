The PHT metadata schema. 
Consists of an OWL ontology for the main defitions and shacl files for constraints. Does not rely on any advanced OWL features, but rather uses OWL because of the availability of editors.

See [https://schema.padme-analytics.de/#] for the newest version.
