<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>

    <meta content="text/html; charset=UTF-8" http-equiv="content-type"></meta>
    <title>PHT Metadata Schema Specification 1.0</title>

    <!-- <link class="required" rel="stylesheet" href="w3c-unofficial.css" type="text/css"/> -->
    <link class="required" rel="stylesheet" href="https://www.w3.org/StyleSheets/TR/w3c-unofficial.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://www.w3.org/StyleSheets/TR/w3c-tr.css" />
    <link rel="stylesheet" type="text/css" href="additional.css" />
</head>
<body>
<div class="content">

<div class="head">

<!-- <p id="logo"></p> -->

<h1>PHT Metadata Schema Specification 1.0</h1>

<h2 id="pagesubtitle"><time datetime="2022-05-18">May 18, 2022</time></h2>

Generated at ${dt}.
<dl>
  <dt>This version:</dt>
  <dd><em><a href="https://schema.padme-analytics.de/">https://schema.padme-analytics.de/</a></em></dd>
  
  <dt>Latest version:</dt>
  <dd><em>URL of previous version</em></dd>
  
  <!--<dt>Previous version:</dt>
  <dd><em>URL of previous version</em></dd>-->
  
  <dt>Editors:</dt>
	<dd><em>Laurenz Neumann</em>, RWTH Aachen</dd>
</dl>


<p class="copyright required">This document is licensed under a <a class="subfoot" href="http://creativecommons.org/licenses/by/3.0/" rel="license">Creative Commons Attribution 3.0 License</a>. </p>

</div>

<hr title="Separator from Header"></hr>

<h2 id="abstract">Abstract</h2>

    <p>
    The PHT Metadata Schema allows to describe various entities in implementations following the Personal Health Train paradigm. The Personal Health Train is an approach to distributed analysis is healthcare, enabling stakeholder to provide their data for scientific computerised analysis while keeping the data soverignity.
    However, due to its highly decentralised architecture, it is obscure to the user and interoperability with different implementations is not granted.
    Therefore, this specification is aimed at creating a unified communication standard as "lingua franca" between different components in one architecture, but also different architectures.
    </p>


    <!--<p class="note"><strong>Note:</strong> This template suggests common sections and considerations for authoring technical specifications.  However, if any section does not seem appropriate for the particular specification being written, or if sections are missing, the author should adapt this specification in any way needed, updating the Table of Contents as necessary.  To avoid confusion with official W3C specifications, W3C asks that passages marked with the class <em>"required"</em> are retained.</p>
    -->

<div id="sotd">
    <h2 id="status">Status of This Document</h2>

    <p class="required"><em>This section describes the status of this document at the time of its publication. Other documents may supersede this document.</em></p>

    <p class="disclaimer required"><strong>Although W3C has provided the template for this document for community use, the document is not affiliated with W3C in any way, and is not endorsed or approved by W3C.</strong></p>

    <p class="required">This document is a draft of this specification, produced by an independent party.  It is inappropriate to cite this specification as as a publication of the World Wide Web Consortium (W3C), or anything other than a <em>work in progress</em> by an independent entity.</p>

    <p>This document is an early developement specification. It is subject to change at any time and should be viewed as a fixed specification. Furthermore, it may lack important parts needed in a formal specification.</p>
    <h3>Intent of this Specification</h3>

    <p>This intent of this document is to provide a specification of the components in the PHT paradigm with a machine-readable vocabulary to enable semantic interoperability.</p>

    <!--<h3>Implementation Experience</h3>

    <p><em>Insert a short description of of existing implementation experience that informs this specification.</em></p>


    <h3>Changes Since the Previous Draft</h3>

    <p><em>Insert a list of substantive changes since the previous version of the specification, or state that no changes have been made.</em></p>-->


    <h3>Level of Endorsement by the Community</h3>

    <p>The <a href="https://padme-analytics.de/">PADME analytics group</a> endorses this schema.</p>


    <h3>How to provide feedback</h3>

    <p>Please send comments on this specification by opening an issue on the gitlab page of the project.</p>

</div>

<hr />


<div id="toc">
	<h2>Contents</h2>
	<ol class="toc">
		<li>1. <a href="#sec-intro">Introduction</a></li>
		<li>2. <a href="#conformance">Conformance</a></li>
		<li>3. <a href="#ucr">Use Cases and Requirements</a>
		<li>4. <a href="#features">Vocabulary Overview</a></li>
		<li>5. <a href="#examples">Vocabulary Specification</a></li>
		<li>6. <a href="#security">Examples</a></li>
		<li>7. <a href="#glossary">Security Considerations</a></li>
		<li>8. <a href="#schema">Glossary</a>
		<li>10. <a href="#Acknowledgments">Acknowledgments</a></li>
	</ol>
</div>



<div id="sec-intro">
	<h2 id="intro">1. Introduction</h2>
	<p>This section is informative.</p>
    <p>An TURTLE version of this schema is available: <a href="https://schema.padme-analytics.de/1_0.ttl">https://schema.padme-analytics.de/1_0.ttl</a></p>
	<em>TODO: Add intro</em>
	<p>The PHT metadata schema is built as an ontology upon OWL.</p>
	<p>The ontology is relativly lightweight, in the sense that it is tried to abstain from complex relationships between classes, such as attribute implicated classes. To nevertheless ensure a sound representation of the data, it is relied on SHACL based validation shapes, which can be employed by all entities utilising the schema.</p>
</div>



<div id="sec-conformance">  
	<h2 id="conformance">2. Conformance</h2>
	<p>This section is normative.</p>
	
	<p><em>Describe different conformance classes here.</em></p>

	<p>This document contains explicit conformance criteria that overlap with some RNG definitions in requirements. If there is any conflict between the two, the explicit conformance criteria are the definitive reference. </p>
	
	<p>Within this specification, the key words "MUST, "MUST NOT", "REQUIRED, "SHALL, "SHALL NOT, "SHOULD, "SHOULD NOT", "RECOMMENDED, "MAY", and "OPTIONAL" are to be interpreted as described in <a href="http://www.ietf.org/rfc/rfc2119.txt">RFC 2119</a> [<a href="ref-RFC2119">RFC2119</a>].  However, for readability, these words do not necessarily appear in uppercase in this specification.</p>

</div>



<div id="sec-ucr">
	<h2 id="ucr">3. Use Cases and Requirements</h2>
	<p>This section is informative.</p>
	
	<h3 id="use-cases">3.1. Use Cases</h3>
  <p>The following usage scenarios illustrate some of the ways in which <em>PHT Metadata Schema</em> might be used for various applications:</p>

  <p><strong>Transparancy for users.</strong>: While the pht enable users to easily tackle distributed analytics, it is a black box once the algorithm is sent away. With the metadata modelled following this schema... </p>
  <p><strong>Insert Use Case 2</strong>: Explain use case 2. </p>

</div>



<div id="sec-vocab-overview">
	<h2 id="features">4. Vocabulary Overview</h2>
	<p>This section is informative.</p>
	
	<p>
		The PHT Metadata Schema is an OWL2 ontology used to describe various entities in a PHT implementation. Each class and property is expressed with an global identifier (IRI).
		Hence, this holds for the entities described. The properties of the classes are further constrained with SHACL descriptions.
    The specification can be viewed from two perspectives: interoperability and observability.
    <p>Interoperability aims at making different implementations of the PHT interoperabale. The main classes for this are State, State Source and Representation.</p>
    <p>Observability aims at making implementations less obscure for the user. This includes in this specification the TripEvent mechanism to allow different entities to share information about trains.</p>
		This schema represents the main entities as classes. Additional classes describe more detailed parts of those entities or their interaction.
		The extend to which the schema is used is leaved to the individual PHT implementations. It can be used as informative only, just increasing the transparancy or can be used an normative, effectivly enabling interoperability and replacing other formats of communication in the implementation.
		All classes introduced in this specification are subclasses of <a href="#PHTConcept">PHTConcept</a>.
    Transparancy information is modelled via <em>Event</em> classes.
	</p>
	<p>The two main entities which interact in a PHT implementation are:</p>
	<ul>
		<li><strong>Train:</strong> A Train is an encapsulated analysis taks, which is sent around different station, interacting with the data stored there. A Train can place various requierements for a station, including technical requierements. A Train has a special format in which it is transferred between stations, however this document does not impose any special constraints on these formats, 
			but rather leaves the decision on those formats to the implementations, providing tools to describing them and enabling interoperability.</li>
		<li><strong>Station:</strong> A Station is an entity holding datasets and offering it to trains for analysis. The data stored in such a station in the form of datasets. Any format of data is allowed, ranging from simple csv files to FHIR databases. What kind of data is provided and supported is up to the PHT implementations.
			It can support the execution of different formats of trains and can place constraints on which trains are able to visit them.</li>
	</ul>

  <figure>
  <img src="resources/schema_classes_overview.png" alt="Overview of the main classes in the schema">
  <figcaption>
  Visualisation of the main classes described in this schema. The classes are grouped based on whether they can be assigned to the station aspect or the train aspect of the infrastructure.
  </figcaption>
  </figure>

  <section id="overview-train">
  <h3>Train</h3>
  The class Train is used to describe an encapsulated analysis task in the PHT paradigm. 
  Train(s) visit stations with a well defined route and interact with the data there. It is possible to define Train Classes as blueprints for new trains.
  In this specification, a Train is perceived as an order of <a href="#overview-state">states</a>, including an initial and final state.
  </section>

  <section id="overview-state">
  <h3>State</h3>
  State is an abstract description of an executable state of a <a href="#overview-train">Train</a> throughout the lifecycle of the Train.. Such a State incoroporates all information about the Train and is able to self-transform itself into a new State by execution on a station. Hence, the State can be perceived as a state transformer itself, taking the current information inside the train and the information in the station as an input.
  Therefore, the execution of a train is defined as a series of state transformations. Also a TrainClass is associated with a state, the initial state of the train. State can be affiliated with a <a href="#overview-representation">Representation</a>, defining a format in which the state exists.
  Each state can be identified with its own identifier, enabling tracebility. The final state of a train normally forms the result of the overall train execution. 
  <figure>
    <img src="resources/exec_as_statetransform.png" alt="Visualisation of the execution as a state transformation.">
    <figcaption>
    Visualisation of an execution over two station as a transformation of states. 
    Each state visit utilises an input State and the (selected) information stored at the station to transform the input state into an output state.
    </figcaption>
  </figure>
  Note that during the lifecycle of a Train, multiple states can exist in parallel. If federated learning is used for example, each station in the federated learning round emits its own intermediate state, which are afterwards transformed into one global state.
  Also, the state of a train may not always be accessible: If a Station transforms a State, the old state may not be accessible anymore, depending on the implementation.
  The Train Class, is also represented by a state, which is used as the input state for the first station.
  </section>

  <section id="overview-representation">
  <h3>Representation</h3>
  Representation describe a concrete serialization of a <a href="#overview-state">State</a> which can be exchanged and executed. An instance of this class describes one Representation of one state. 
  Several subclasses of this class exist to describes different Representations. The relationship between States and Representations is 1 to many, i.e. one State can have different Representations in parallel. This is useful, if a Station has no information about the 
  compability of the next station in an execution and want to provide different Representations for the next station to choose from.
  Each Representation should be affiliated with a <a href="#overview-statesource">State Source</a>.

  Some Representation subclasses recursivly consists out of other Representation instances to describe more complex serialization, e.g. where the data-holding and algorithmic part are separatly stored. 
  </section>

  <section id="overview-statesource">
  <h3>State Source</h3>
  Instances of the State Source determine where a Representation of a State is stored and how it can be retrieved.
  It includes the protocol and also the location of any handlers or endpoints needed for a successful transfer.
  The different ways of storing or retrieving a <a href="#overview-train">Train</a> are expressed as subclasses.
  It does not describe a source in the sense that it is one interface for retrieving different states from a collection, but rather a source which is bound to one specific 
  <a href="#overview-representation">Representation</a> of a state and describes retrieval methods for this Representation only.
  </section>

  <figure>
  <img src="resources/overview_state_repr_source.png" alt="Abstract visualisation of the classes State, Representation and State Source.">
  <figcaption>
  Abstract Visualisation of an exemplary usage of the State-related classes. A State is available as a self-contained, monolithic docker image. This image 
  is sourced in a Registry service, with a well-defined endpoint, where user can retrieve this Representation.
  </figcaption>
  </figure>

  <section id="overview-events">
  <h3>Events</h3>
      <p>
          To enable transparancy for the PHT paradigm, Events are used, statements which should be true at a specific point in time. Based on these events, users are able to retrace the execution of a train and the interaction between different entities.
          The Events describe a piece of information about the Train in a fixed point in Time. This includes Resource consumption at Stations, which is communicated to the Train Owner to be able to retrace the performance.
          Also, changes of the lifecycle state of the execution, such as whether a Train has started running or finished at a specific Station can be expresses with such events.
          Beside fulfilling tasks of conveying information, Events can also be used for inter-entity communication, for example to be able to express state changes to other stations.
          The mechanisms for exchanging the state information is not defined in this document.
      </p>
      <figure>
      <img src="resources/state_lifecycle_events.png">
      <figcaption>
      Visualisation of the events w.r.t the lifecycle of States. If the event is anotated at an edge, it means that this event is created on the transition describes by that edge.
      </figcaption>
      </figure>
      <p>
          You can broadly divide the different events in two different categories: node based and edge based (see Figure).
      </p>
      <p>
          Edge based events describe the change of one lifecycle state to another and thus define the lifecycle state of a train at a specific point in time implicitly.
      </p>
      <p>
          Node based events contain information about a state of a train throughout the time of an implicit lifecycle state. They should be bound to specific lifecycle state.
      </p>
      <h4>Event association</h4>
      <p>
          Important is the association of the events with the other resources in this specification.
          Each event is associated with a state. The semantic of this association is defined by each Event subclass. Additionally, each event is associated with the Informational Authority which emitted this event.
          <br>For validation of an event, the association of the event with the Informational Authority should be used to check whether the event is proofed to be emmited by the specified entity. This should be communicated to the user
          if any visualisation based on the events is happening.
      </p>
  </section> 

  <section id="overview-configuration">
  <h3>Configurations</h3>
    <p>
    The Configuration classes allow to express necessary configuration for <a href="#Train">Trains</a> and Data Interfaces. The two main Classes are <a href="#ConfigurationType">Configuration Type</a> and <a href="#ConfigurationTemplate">Configuration Template</a>.
    Configuration Type is used to describe the semantic of a specific configuration parameter. The schema is shipped with multiple instances describing different semantics such as username, password, etc..
    Configuration Template describes the means how a parameter is conveyed to a Train in a Station. The schema ships multiple subclasses to describe different means of communicating those parameters, e.g. via environment variable.
    Data Interfaces may also point to instaces of Configuration Type to describe, that the described Configuration parameter is needed to utilise the Data Interface. 
    Since, both the ConfigurationTemplate of Trains and Station utilise the same instances for describing the semantic of configuration, they can be matched.
    If a Data Interace has a static configuration which value can be made publice, such as a port used, the <a href="#FixedValueConfiguration">Fixed Value Configuration</a> can be utilised.
    </p>
    <h4>
    </h4>
  </section>

  <h3>Station</h3>
  <section id="overview-station">
  The class Station is used to describe a Station in the context of the PHT paradigm. A Station provides at least one <a href="#ExecutionEnvironment">Execution Environment</a> for the execution of Trains. It can also provide a DataSet to which it may offers access.  
  Each Station needs to be associated with an Owner. See <a href="sec-ownership">Ownership</a> for more details.
  </section>

  <h3>Execution Environment</h3>
  <section id="overview-exec-env-deps">
  The Execution Environment class describes some virtual or actual environment for execution of Trains. Each of these Environments can support specific <a href="#Dependency">Dependencies</a>. The Dependecies express on the Station side
  some condition that is met by the Execution Environment. On the Train side these dependecies express conditions that has to be fulfilled so the train can be executed. Execution Environments also determine what kind of representations they support for execution. They do this by providing a list of subclasses of the <a href="#Representation">Representation</a> class.
  <h4>Dependencies</h4>
  The Dependency class can be used two-fold: In the context of a Train Class, it describes what kind of dependency the Train Class necessitates for an execution of a corrosponding instance.
  In the context of a Station, the class can be used to denote what dependencies the station fulfills. 
  To enable this notation of minimum-requierement, a total order is implicitly defined on each of the Dependecies subclasses. 
  Then, in the context of Station, an instance of the Dependency class denotes the maximum supported.
  In the context of a Train, an instance of the Dependency class denotes the minimum needed.
  For example, an instance of <a href="#StorageDependency">Storage Dependency</a> with a value of 1000 means in the context of a Station, that it supports Trains with a maximum size of 1000 MB.
  In the context of a Train this would mean that the Train needs at least 1000 MB storage space to be executed.
  </section>

  
	
	
</div>

<div id="sec-vocab-specification">
	<h2 id="features">5. Vocabulary Specification</h2>
	<p>This section is normative.</p>
	<h3>Elements from other vocabularies.</h3>
	This specification utilises the DCAT vocabulary by extending some of the concepts introduced in DCAT.
  <h3> Class Specifications </h3>

  % for class_def in classes:
    <section id="${class_def.get_defining_part()}">
      <h4 class="vocab_specification_classname">Class: ${class_def.get_label()}</h4>
        <table class="class_definition_table">
        <thead>
          <tr><th>RDF Class</th><th>${class_def.get_CURIE()}</th></tr>
        </thead>
        <tbody>
        % for definition in class_def.definitions:
        <tr><td>Definition</td><td>${definition}</td><tr>
        % endfor
        % for comment in class_def.comments:
        <tr><td>Comment</td><td>${comment}</td><tr>
        % endfor
        <tr></tr>
        </tbody>
        </table>
      % if len(class_def.direct_superclasses) > 0:
        <p>The class has the following direct superclasses:</p>
        % for superc in class_def.direct_superclasses:
          <a href=${str(superc.iri)}>${superc.get_label()}</a>
        %endfor
      % endif
      % if len(class_def.object_properties) > 0:
      <p>The class has the following object properties:</p>
      <p>
        % for object_prop in class_def.object_properties:
        <a href=${str(object_prop.iri)}>${object_prop.get_defining_part()}</a>,
        % endfor
      </p>
      % endif
      % if len(class_def.data_properties) > 0:
      <p>The class has the following data properties:</p>
      <p>
        % for data_prop in class_def.data_properties:
        <a href=#${data_prop.get_defining_part()}>${data_prop.get_defining_part()}</a>,
        % endfor
      </p>
      % endif
    </section>
  % endfor 

  <h3> Object Property Specifications </h3>

  % for obj_prop in object_properties:
    <section id="${obj_prop.get_defining_part()}">
    <h4 class="vocab_definition_object_prop_name">Object Property: ${obj_prop.get_defining_part()}</h4>
    <table class="object_property_definition_table">
    <thead>
      <tr><th>Property</th><th>${obj_prop.get_label()}
    </thead>
    <tbody>
    % for definition in obj_prop.definitions:
    <tr><td>Definition</td><td>${definition}</td>
    % endfor
    % for comment in obj_prop.comments:
    <tr><td>Comment</td><td>${comment}</td>
    % endfor
    </tbody>
    </table>
    </section>
  % endfor
</div>



<div id="sec-examples">
	<h2 id="examples">6. Examples</h2>
	<p>This section is informative.</p>
	
	<p>
  <a href="/examples/train_padme.ttl">Example 1</a> is an example which models a train visiting three station in the PADME implementation. In this example a train has a route, designating a visit to three stations. The train already visited 2 of these station in this example.
  Each of the visits yielded a state which are described in terms of format and where to find them. Note that this would be optional in this case, since for an execution in one implementation, the program logic of the implementation would normally deal with such information.
  </p>
  <p>
  <a href="/examples/train_inter_implementation.ttl">Example 2</a> is an example which describes a train executed in two different implementation consecutively.
	</p>
  <p>
  <a href="/examples/station.ttl">Example 3</a> is a description of a station with a Dataset containing basic patient information and one execution environment.
  </p>
</div>



<div id="sec-security">
	<h2 id="security">7. Security Considerations</h2>
	<p>This section is informative.</p>

	<p>Currently, Security Considerations are not part of this specification.</p>
	
</div>



<div id="sec-glossary">
	<h2 id="glossary">8. Glossary</h2>
	<p>This section is normative.</p>

	<table border="1px">
		<tr><th>Term</th><th>Definition</th></tr>
		<tr><td>PHT implementation</td><td>A PHT implementation is an implementation of the necessary architectural components to perform analysis follwing the PHT paradigm.</td></tr>
    <tr><td>lifecycle state</td><td> A lifecycle state denotes the implicit state of the train, e.g. whether it is currently running, idlying...</td></tr>
	</table>
	
</div>




<div id="sec-refs">
	<h2 id="refs">9. References</h2>

	<h3 id="normrefs">9.1. Normative References</h3>
	<dl>

    <dt id="ref-RFC2119"><strong class="normref">[RFC2119]</strong></dt>
    <dd>
      <cite><a href="http://tools.ietf.org/html/rfc2119">Key words for use in RFCs to Indicate Requirement Levels</a></cite>,
      S. Bradner, March 1997.
      <br />Available at http://tools.ietf.org/html/rfc2119.
    </dd>

    <dt id="ref-RNG"><strong class="normref">[RELAXNG]</strong></dt>
    <dd>
      <cite><a href="http://www.y12.doe.gov/sgml/sc34/document/0362_files/relaxng-is.pdf">Document Schema Definition Languages (DSDL) — Part 2: Regular grammar-based validation — RELAX NG, ISO/IEC FDIS 19757-2:2002(E)</a></cite>,
      J. Clark, <ruby><rb xml:lang="ja">村田 真</rb> <rp>(</rp><rt><span class="familyname">Murata</span> M.</rt><rp>)</rp></ruby>, eds.
      International Organization for Standardization, 12 December 2002.
      <br/>Available at http://www.y12.doe.gov/sgml/sc34/document/0362_files/relaxng-is.pdf.
    </dd>

    <dt><a id="ref-XMLSCHEMA" class="normref">[XMLSCHEMA]</a></dt>
    <dd>
      <cite><a href="http://www.w3.org/TR/2004/REC-xmlschema-1-20041028/">XML Schema Part 1: Structures Second Edition</a></cite>, 
      H. S. Thompson <em xml:lang="la" lang="la">et al.</em>, <abbr title="editors">eds.</abbr>, 
      W3C Recommendation, 28 October 2004.<br/>
      Available at: http://www.w3.org/TR/2004/REC-xmlschema-1-20041028/<br/>
    
      <cite><a href="http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/">XML Schema Part 2: Datatypes Second Edition</a></cite>, 
      P. V. Biron, A. Malhotra, <abbr title="editors">eds.</abbr>, 
      W3C Recommendation, 28 October 2004.<br/>
      Available at: http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/
    </dd>

	</dl>

	<h3 id="informrefs">9.2. Informative References</h3>
	<dl>

    <dt id="ref-SCHEMA2"><strong class="informref">[SCHEMA2]</strong></dt>
    <dd>
      <cite class="w3crec"><a href="http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/">XML Schema Part 2: Datatypes Second Edition</a></cite>.
      P. Biron, A. Malhotra, eds.
      World Wide Web Consortium, 28 October 2004.
      (See also <a href="http://www.w3.org/TR/2005/NOTE-xml11schema10-20050511/"><cite>Processing XML 1.1 documents with XML Schema 1.0 processors</cite></a>
      [<a href="#ref-XML11-SCHEMA">XML11-SCHEMA</a>].)
      <br />This edition of XML Schema Part 2 is http://www.w3.org/TR/2004/REC-xmlschema-2-20041028/.
      <br />The <a href="http://www.w3.org/TR/xmlschema-2/">latest edition of XML Schema Part 2</a> is available at
      http://www.w3.org/TR/xmlschema-2/.
    </dd>
  
    <dt id="ref-WebIDL"><strong class="informref">[WebIDL]</strong></dt>
    <dd>
      <cite class="w3cwd"><a href="http://www.w3.org/TR/2008/WD-WebIDL-20081219/">WebIDL</a></cite>,
      C. McCormack, ed.
      World Wide Web Consortium, <span class="wip">work in progress</span>, 19 December 2008.
      <br />This edition of WebIDL is http://www.w3.org/TR/2008/WD-WebIDL-20081219/.
      <br />The <a href="http://dev.w3.org/2006/webapi/WebIDL/">latest edition of WebIDL</a> is available at
      http://dev.w3.org/2006/webapi/WebIDL/.
    </dd>

	</dl>
	
</div>


</div>
</body>
</html>