from setuptools import setup, find_packages
from pathlib import Path

setup(
    name="specificationsynth",
    version="1.0",
    author="Laurenz Neumann",
    author_email="laurenz.neumann@rwth-aachen.de",
    packages=find_packages(),
    install_requires=["rdflib~=6.2.0"]
)