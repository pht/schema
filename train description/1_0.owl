@prefix : <https://schema.padme-analytics.de/train_description#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix pht: <http://schema.padme-analytics.de#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@base <https://schema.padme-analytics.de/train_description#> .

<https://schema.padme-analytics.de/train_description#> rdf:type owl:Ontology ;
                                                        owl:imports <http://schema.padme-analytics.de/1.0> .

#################################################################
#    Object Properties
#################################################################

###  https://schema.padme-analytics.de/train_description#ReferencesLocationInFile
:ReferencesLocationInFile rdf:type owl:ObjectProperty ;
                          rdfs:subPropertyOf pht:PHTObjectProperty ;
                          rdfs:domain :FileLocation ;
                          rdfs:range :TrainFile ;
                          rdfs:label "references Location in File"@en ;
                          <http://www.w3.org/ns/prov#definition> "Associates a FileLocation with the File this Location is referencing."@en .


###  https://schema.padme-analytics.de/train_description#TrainClassHasConfigurationFile
:TrainClassHasConfigurationFile rdf:type owl:ObjectProperty ;
                                rdfs:subPropertyOf pht:PHTObjectProperty ;
                                rdfs:domain pht:TrainClass ;
                                rdfs:range :TrainConfigurationFile ;
                                rdfs:label "Train Class has configuration file."@en .


###  https://schema.padme-analytics.de/train_description#collectionHasFile
:collectionHasFile rdf:type owl:ObjectProperty ;
                   rdfs:subPropertyOf pht:PHTObjectProperty ;
                   rdfs:domain :FileCollection ;
                   rdfs:range :TrainFile ;
                   <http://www.w3.org/ns/prov#definition> "Associates a collection with a file that is part of that collection."@en .


###  https://schema.padme-analytics.de/train_description#containsSyntacticalComponent
:containsSyntacticalComponent rdf:type owl:ObjectProperty ;
                              rdfs:subPropertyOf pht:PHTObjectProperty ;
                              rdfs:domain :TrainFile ;
                              rdfs:range :SyntacticalComponent ;
                              <http://www.w3.org/ns/prov#definition> "Describes that the TrainFile contains a syntactical component. The location of that component in that file is describes with a FileLocation instances associated with the component."@en .


#################################################################
#    Data properties
#################################################################

###  https://schema.padme-analytics.de/train_description#fileLocationEndingOffset
:fileLocationEndingOffset rdf:type owl:DatatypeProperty ;
                          rdfs:domain :FileLocation ;
                          rdfs:range xsd:integer ;
                          <http://www.w3.org/ns/prov#definition> "Describes the offset in the ending row at which fracture of the file the FileLocation references end. This attribute is optional, if it is not defined, it is assumed that the whole ending row belongs to the location which is referenced"@en .


###  https://schema.padme-analytics.de/train_description#fileLocationEndingRow
:fileLocationEndingRow rdf:type owl:DatatypeProperty ;
                       <http://www.w3.org/ns/prov#definition> "Describes the row at which fracture of the file the FileLocation references ends. This attribute is optional, if it is not defined, it is assumed that the FileLocation references a single row."@en .


###  https://schema.padme-analytics.de/train_description#fileLocationStartingOffset
:fileLocationStartingOffset rdf:type owl:DatatypeProperty ;
                            <http://www.w3.org/ns/prov#definition> "Describes the offset in the starting row at which fracture of the file the FileLocation references starts. This attribute is optional, if it is not defined, it is assumed that the whole starting row belongs to the location which is referenced"@en .


###  https://schema.padme-analytics.de/train_description#fileLocationStartingRow
:fileLocationStartingRow rdf:type owl:DatatypeProperty ;
                         <http://www.w3.org/ns/prov#definition> "Describes the row at which fracture of the file the FileLocation references starts."@en .


#################################################################
#    Classes
#################################################################

###  https://schema.padme-analytics.de/train_description#AlgorithmFiles
:AlgorithmFiles rdf:type owl:Class ;
                rdfs:subClassOf :FileCollection ;
                rdfs:label "Algorithm Files"@en ;
                <http://www.w3.org/ns/prov#definition> "The collection of files in a TrainClass containing the executable part. This files should not change during the execution of a train."@en .


###  https://schema.padme-analytics.de/train_description#Comment
:Comment rdf:type owl:Class ;
         rdfs:subClassOf :SyntacticalComponent .


###  https://schema.padme-analytics.de/train_description#Dockerfile
:Dockerfile rdf:type owl:Class ;
            rdfs:subClassOf :TrainConfigurationFile ;
            rdfs:label "Dockerfile"@en ;
            <http://www.w3.org/ns/prov#definition> "Describes a Train Configuration File in the form of a Dockerfile."@en .


###  https://schema.padme-analytics.de/train_description#FileCollection
:FileCollection rdf:type owl:Class ;
                rdfs:subClassOf pht:Concept ;
                <http://www.w3.org/ns/prov#definition> "A collection of files inside a Train or a TrainClass which are necessary for a specific part of the Train or the TrainClass."@en .


###  https://schema.padme-analytics.de/train_description#FileLocation
:FileLocation rdf:type owl:Class ;
              rdfs:subClassOf pht:Concept ;
              rdfs:label "File Location"@en ;
              <http://www.w3.org/ns/prov#definition> "Used to reference a fracture of a file. The referenced file is associated via a property."@en .


###  https://schema.padme-analytics.de/train_description#FunctionCall
:FunctionCall rdf:type owl:Class ;
              rdfs:subClassOf :SyntacticalComponent .


###  https://schema.padme-analytics.de/train_description#Parameter
:Parameter rdf:type owl:Class ;
           rdfs:subClassOf :SyntacticalComponent .


###  https://schema.padme-analytics.de/train_description#SyntacticalComponent
:SyntacticalComponent rdf:type owl:Class ;
                      rdfs:subClassOf pht:Concept .


###  https://schema.padme-analytics.de/train_description#TrainConfigurationFile
:TrainConfigurationFile rdf:type owl:Class ;
                        rdfs:subClassOf :TrainFile ;
                        rdfs:label "Train Configuration File"@en ;
                        <http://www.w3.org/ns/prov#definition> "Describes a File associated with the TrainClass, which is only used during creation of the TrainClass."@en .


###  https://schema.padme-analytics.de/train_description#TrainFile
:TrainFile rdf:type owl:Class ;
           rdfs:subClassOf pht:Concept ;
           <http://www.w3.org/ns/prov#definition> "Describes a File associated with a Train or a TrainClass."@en .


###  Generated by the OWL API (version 4.5.9.2019-02-01T07:24:44Z) https://github.com/owlcs/owlapi
