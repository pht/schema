# generate html file
FROM python:3.8
RUN pip install mako rdflib
COPY specificationsynth/ specificationsynth/
COPY shapes/ shapes/
COPY setup.py setup.py
RUN pip install .
COPY 1_0.mako 1_0.mako
COPY 1_0.owl 1_0.owl
COPY spec_generator.py spec_generator.py
RUN python3 spec_generator.py

# build the webserver from nginx baseline
FROM nginx:latest

COPY --from=0 rendered_html.html /usr/share/nginx/html/index.html
COPY 1_0.owl /usr/share/nginx/html/1_0.ttl
COPY additional.css /usr/share/nginx/html/additional.css
COPY examples/ /usr/share/nginx/html/examples/
COPY resources /usr/share/nginx/html/resources/